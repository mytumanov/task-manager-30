package ru.mtumanov.tm.component;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import lombok.SneakyThrows;
import ru.mtumanov.tm.command.AbstractCommand;

public class FileScanner extends Thread {

    @NotNull
    private Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> abstractCommands = bootstrap.getCommandService().getCommandsWithArgument();
        abstractCommands.forEach(e -> this.commands.add(e.getName()));
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            process();
        }
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    Files.delete(file.toPath());
                    bootstrap.processCommand(fileName, false);
                } catch (@NotNull Exception e) {
                    bootstrap.getLoggerService().error(e);
                }
            }
        }
    }
    
}

package ru.mtumanov.tm.component;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.jetbrains.annotations.NotNull;

import lombok.SneakyThrows;
import ru.mtumanov.tm.command.data.AbstractDataCommand;
import ru.mtumanov.tm.command.data.DataBase64LoadCommand;
import ru.mtumanov.tm.command.data.DataBase64SaveCommand;
import ru.mtumanov.tm.exception.AbstractException;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        try {
            bootstrap.processCommand(DataBase64SaveCommand.NAME, false);
        } catch (@NotNull AbstractException e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64)))
            return;
        try {
            bootstrap.processCommand(DataBase64LoadCommand.NAME, false);
        } catch (@NotNull AbstractException e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }
    
}

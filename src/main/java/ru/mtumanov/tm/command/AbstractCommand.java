package ru.mtumanov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.model.ICommand;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.AccessDeniedException;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public String getUserId() throws AbstractException {
        @Nullable
        String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null)
            throw new AccessDeniedException();
        return userId;
    }

    @Override
    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty())
            result += name + " : ";
        if (argument != null && !argument.isEmpty())
            result += argument + " : ";
        if (!description.isEmpty())
            result += description;
        return result;
    }

}

package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}

package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-arg";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show argument list";
    }

    @Override
    @NotNull
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        System.out.println("[Arguments]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) continue;
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}

package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.ProjectSort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show list projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        Comparator<Project> comparator = null;
        if (sort != null)
            comparator = sort.getComparator();
        int index = 1;
        @NotNull final List<Project> projects = getProjectService().findAll(userId, comparator);
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}

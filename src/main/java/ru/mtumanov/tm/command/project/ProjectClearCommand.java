package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final String userId = getUserId();
        getProjectService().clear(userId);
    }

}

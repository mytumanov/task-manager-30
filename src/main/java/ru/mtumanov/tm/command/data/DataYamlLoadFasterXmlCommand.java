package ru.mtumanov.tm.command.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.data.DataLoadException;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from yaml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-yaml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
            @NotNull final String yaml = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
            setDomain(domain);
        } catch (IOException e) {
            throw new DataLoadException();
        }
    }
    
}

package ru.mtumanov.tm.command.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.data.DataSaveException;

public class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to xml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException | JAXBException e) {
            throw new DataSaveException();
        }
    }
    
}

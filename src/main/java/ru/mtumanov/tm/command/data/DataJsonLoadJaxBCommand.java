package ru.mtumanov.tm.command.data;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.data.DataLoadException;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-json-jaxb";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        try {
            @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (JAXBException e) {
            throw new DataLoadException();
        }
    }
    
}

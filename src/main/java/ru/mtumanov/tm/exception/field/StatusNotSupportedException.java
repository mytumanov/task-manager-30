package ru.mtumanov.tm.exception.field;

public class StatusNotSupportedException extends AbstractFieldException {

    public StatusNotSupportedException() {
        super("ERROR! Status is not supported!");
    }

}

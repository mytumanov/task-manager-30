package ru.mtumanov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.enumerated.Status;

public interface IHaveStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}

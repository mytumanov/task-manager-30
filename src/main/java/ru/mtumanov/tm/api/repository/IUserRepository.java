package ru.mtumanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException;

    @NotNull
    User findByEmail(@NotNull String email) throws AbstractException;

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}

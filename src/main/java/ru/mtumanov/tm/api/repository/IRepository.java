package ru.mtumanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    M findOneById(@NotNull String id) throws AbstractEntityNotFoundException;

    @NotNull
    M findOneByIndex(@NotNull Integer index);

    @NotNull
    M remove(@NotNull M model);

    @NotNull
    M removeById(@NotNull String id) throws AbstractEntityNotFoundException;

    @NotNull
    M removeByIndex(@NotNull Integer index) throws AbstractEntityNotFoundException;

    void clear();

    int getSize();

    boolean existById(@NotNull String id);

}

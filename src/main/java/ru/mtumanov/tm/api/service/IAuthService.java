package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException;

    void login(@NotNull String login, @NotNull String password) throws AbstractException;

    void logout();

    boolean isAuth();

    @Nullable
    String getUserId() throws AbstractException;

    @NotNull
    User getUser() throws AbstractException;

    void checkRoles(@Nullable Role[] roles) throws AbstractException;

}
